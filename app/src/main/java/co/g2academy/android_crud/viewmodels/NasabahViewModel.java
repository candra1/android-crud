package co.g2academy.android_crud.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import co.g2academy.android_crud.model.Nasabah;
import co.g2academy.android_crud.model.NasabahResponse;
import co.g2academy.android_crud.model.NasabahsResponse;
import co.g2academy.android_crud.networking.NasabahsRepository;

public class NasabahViewModel extends ViewModel {
    private MutableLiveData<NasabahsResponse> mutableLiveData;
    private NasabahsRepository nasabahsRepository;
    private MutableLiveData<NasabahResponse> mutableNasabahLiveData;
//    private NasabahRepository nasabahRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        nasabahsRepository = NasabahsRepository.getInstance();
        mutableLiveData = nasabahsRepository.getNasabahs("1", "10");
    }

    public LiveData<NasabahsResponse> getNasabahsRepository() {
        return mutableLiveData;
    }
    public void refresh(String page, String limit ){
        if (mutableLiveData != null){
            mutableLiveData = nasabahsRepository.getNasabahs(page, limit);
            return;
        }
        nasabahsRepository = NasabahsRepository.getInstance();
        mutableLiveData = nasabahsRepository.getNasabahs("1", "10");
    }

    public LiveData<NasabahResponse> getNasabahRepository(String id) {
        if (mutableNasabahLiveData == null){
            nasabahsRepository = NasabahsRepository.getInstance();
            mutableNasabahLiveData = nasabahsRepository.getNasabah(id);
        }
        return mutableNasabahLiveData;
    }

    public LiveData<NasabahResponse> putNasabahRepository(String id, Nasabah nasabahPayload) {
        if (mutableNasabahLiveData == null) {
            nasabahsRepository = NasabahsRepository.getInstance();
        }
        mutableNasabahLiveData = nasabahsRepository.updateNasabah(id, nasabahPayload);

        return mutableNasabahLiveData;
    }
    public LiveData<NasabahResponse> postNasabahRepository(Nasabah nasabahPayload) {
        if (mutableNasabahLiveData == null) {
            nasabahsRepository = NasabahsRepository.getInstance();
        }
        mutableNasabahLiveData = nasabahsRepository.postNasabah(nasabahPayload);

        return mutableNasabahLiveData;
    }
    public LiveData<NasabahResponse> deleteNasabahRepository(String id) {
        if (mutableNasabahLiveData == null) {
            nasabahsRepository = NasabahsRepository.getInstance();
        }
        mutableNasabahLiveData = nasabahsRepository.deleteNasabah(id);

        return mutableNasabahLiveData;
    }

}
