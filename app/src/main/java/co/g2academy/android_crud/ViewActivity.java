package co.g2academy.android_crud;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import co.g2academy.android_crud.model.Nasabah;
import co.g2academy.android_crud.viewmodels.NasabahViewModel;


public class ViewActivity extends AppCompatActivity {
    private String id,nama,alamat,email;
    private Nasabah nasabah=new Nasabah();
    
    EditText namaEditText,alamatEditText,emailEditText;
    Button saveButton,deleteButton;
    String mode="add";
    NasabahViewModel nasabahViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        findViewById();
        initData();
        onClickGroup();
    }
    void findViewById(){
        namaEditText = (EditText) findViewById(R.id.namaEditText);
        alamatEditText = (EditText) findViewById(R.id.alamatEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        
        saveButton = (Button) findViewById(R.id.saveButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
    }
    void initData(){
        nasabahViewModel = ViewModelProviders.of(this).get(NasabahViewModel.class);
        Bundle bundle = getIntent().getExtras();
        mode=bundle.getString("mode","add");
        id=bundle.getString("id","");
        nama=bundle.getString("nama","");
        alamat=bundle.getString("alamat","");
        email=bundle.getString("email","");
        Toast.makeText(getApplicationContext(),nama, Toast.LENGTH_LONG).show();

        if (mode.equals("edit")){
            getNasabah(id);
        }

    }
    void onClickGroup(){

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Nasabah nasabahPayload = new Nasabah();
                nasabahPayload.setNama(namaEditText.getText().toString());
                nasabahPayload.setAlamat(alamatEditText.getText().toString());
                nasabahPayload.setEmail(emailEditText.getText().toString());
                if (mode.equals("edit"))
                    putNasabah(id, nasabahPayload);
                else postNasabah(nasabahPayload);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteNasabah(id);
                finish();
            }
        });
    }

    private void getNasabah(String id ){
        nasabahViewModel.getNasabahRepository(id).observe(this, nasabahResponse -> {
            nasabah = nasabahResponse.getData();
            loadData();
        });
    }

    void loadData(){
        namaEditText.setText(nasabah.getNama());
        alamatEditText.setText(nasabah.getAlamat());
        emailEditText.setText(nasabah.getEmail());
    }

    private void putNasabah(String id, Nasabah nasabahPayload ){
        nasabahViewModel.putNasabahRepository(id, nasabahPayload).observe(this, nasabahResponse -> {
            nasabah = nasabahResponse.getData();
            finish();
        });
    }

    private void postNasabah(Nasabah nasabahPayload ){
        nasabahViewModel.postNasabahRepository(nasabahPayload).observe(this, nasabahResponse -> {
            nasabah = nasabahResponse.getData();
            finish();
        });
    }

    private void deleteNasabah(String id){
        nasabahViewModel.deleteNasabahRepository(id).observe(this, nasabahResponse -> {
            nasabah = nasabahResponse.getData();
            finish();
        });
    }

}
