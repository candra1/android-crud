package co.g2academy.android_crud.networking;

import com.google.gson.JsonObject;

import java.util.HashMap;

import co.g2academy.android_crud.model.Nasabah;
import co.g2academy.android_crud.model.NasabahsResponse;
import co.g2academy.android_crud.model.NasabahResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NasabahApi {
    @GET("nasabah")
    Call<NasabahsResponse> getNasabahsList(@Query("page") String page,
                                       @Query("limit") String limit);

    @POST("nasabah")
    Call<NasabahResponse> postNasabah(@Body Nasabah body);

    @GET("nasabah/{id}")
    Call<NasabahResponse> getNasabah(@Path("id") String id);

    @PUT("nasabah/{id}")
    Call<NasabahResponse> putNasabah(@Path("id") String id, @Body Nasabah body);

    @DELETE("nasabah/{id}")
    Call<NasabahResponse> deleteNasabah(@Path("id") String id);

}
