package co.g2academy.android_crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.g2academy.android_crud.adapter.NasabahAdapter;
import co.g2academy.android_crud.model.Nasabah;
import co.g2academy.android_crud.viewmodels.NasabahViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<Nasabah> nasabahArrayList = new ArrayList<>();
    NasabahAdapter nasabahAdapter;
    RecyclerView rvNasabah;
    NasabahViewModel nasabahViewModel;
    TextView refreshTextView, addTextView;
    List<Nasabah> nasabahs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById();
        onClickGroup();
        initData();
    }

    void findViewById(){
        rvNasabah = findViewById(R.id.nasabahRecyclerView);
        refreshTextView = (TextView) findViewById(R.id.refreshTextView);
        addTextView = (TextView) findViewById(R.id.addTextView);
    }

    private void initData() {
        if (nasabahAdapter == null) {
            nasabahAdapter = new NasabahAdapter(MainActivity.this, nasabahArrayList);
            rvNasabah.setLayoutManager(new LinearLayoutManager(this));
            rvNasabah.setAdapter(nasabahAdapter);
            rvNasabah.setItemAnimator(new DefaultItemAnimator());
            rvNasabah.setNestedScrollingEnabled(true);
        } else {
            nasabahAdapter.notifyDataSetChanged();
        }
        nasabahViewModel = ViewModelProviders.of(this).get(NasabahViewModel.class);

        nasabahViewModel.init();
        nasabahViewModel.getNasabahsRepository().observe(this, nasabahsResponse -> {
            nasabahs = nasabahsResponse.getData();
            nasabahArrayList.clear();
            nasabahArrayList.addAll(nasabahs);
            nasabahAdapter.notifyDataSetChanged();
        });


    }
    private void getListNasabah(String page, String limit ){
        nasabahViewModel.refresh(page,limit);
        nasabahViewModel.getNasabahsRepository().observe(this, nasabahsResponse -> {
            nasabahs = nasabahsResponse.getData();
            nasabahArrayList.clear();
            nasabahArrayList.addAll(nasabahs);
            nasabahAdapter.notifyDataSetChanged();
        });
    }
    void onClickGroup(){
        refreshTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getListNasabah("1","20");
            }
        });
        addTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent( getApplicationContext(), ViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("mode", "add");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getListNasabah("1","20");
    }
}